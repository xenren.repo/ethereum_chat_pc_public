import axios from 'axios'
import config from '@/config'
const ENV = process.env.VUE_APP_ENV
let _axios = axios.create({
    baseURL: config[ENV].host,
    responseType: 'blob',
  
    
})

_axios.interceptors.request.use(
    function (config) {
        let _token = sessionStorage.token || ''
        config.headers.Authorization = `Bearer ${_token}`;

        return config
    },
    function (error) {
        return Promise.reject(error)
    }
)


export default _axios

