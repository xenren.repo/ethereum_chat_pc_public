import axios from 'axios'
import store from '@/store'
import config from '@/config'
import router from '../router/index'
import { Message } from 'element-ui'

const ENV = process.env.VUE_APP_ENV
export default (Vue) => {
  Object.defineProperties(Vue.prototype, {
    $http: {
      value: axios
    }
  })
  
  // axios配置
  axios.defaults.baseURL = config[ENV].host
  axios.defaults.timeout = 0
  axios.defaults.withCredentials = true
  //  添加拦截器
  // request拦截器
  axios.interceptors.request.use(function (config) {
    let _token = sessionStorage.token || ''
    config.headers.Authorization = `Bearer ${_token}`;
    store.dispatch('setLoading', true)
    return config
  }, function (error) {
    store.dispatch('setLoading', false)
    return Promise.reject(error)
  })

//response 拦截器
  axios.interceptors.response.use(function (response) {
    store.dispatch('setLoading', false)
    if (Number(response.data.code) !== 0) {
      Message({
        message: response.data.message,
        type: 'error',
        duration: 2 * 1000
      })
      if(Number(response.data.code)===1000){
          sessionStorage.clear()
          if(response.config.url.indexOf('order')!=-1){
            sessionStorage.goOrder = 1
          }
          
          router.push('/login')
          console.log('超时') 
      }
      return Promise.reject(response.data)
    } else {
      return Promise.resolve(response.data)
    }
  }, function (error) {
    store.dispatch('setLoading', false) 
    if (error.response) {
      console.log(error.response.status)
      switch (error.response.status) {
        case 401:
          console.log('401 未授权')
          break
        case 500:
          console.log('账号密码出错')
          break

        default:
          console.log('发生错误了')
      }
    } else {
      console.log('超时')
    }
    Message.error('呀呀呀，系统出错了')
    return Promise.reject(error)
  })
}
