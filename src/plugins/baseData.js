
const baseData = () => {
    return {
        status: [{ name: '启用', id: '0' }, { name: '禁用', id: '1' }, { name: 'under development', id: '2' }],
        goodsClassType: [{ name: '外部分类', id: '0' }, { name: '自营品牌分类', id: '1' }],
        is: [{ name: '是', id: '1' },{ name: '否', id: '0' }],
        on_sale:[{ name: '上架', id: '1' },{ name: '下架', id: '0' }],
        goodsType:[{ name: '实体', id: '1' },{ name: '虚拟', id: '2' }],
        filterGoodsByCreator: [{ name: '后台', id: '1' },{ name: '商家', id: '2' }]
    }


}

export default {
    data() {
        return {
            locBaseData: baseData()
        }
    },

    filters: {
        getLocBaseData(v, key) {
            let bd = baseData()[key]
            let item = bd.find(i => i.id == v)
            if (item) { return item.name }
            return '未知'
        },

        statusClass(v) {
            if (v == 0) { return 'm_green' }
            if (v == 1) { return 'm_red' }
        },


        m_isAdd(v) {
           return v?'添加':'编辑'

        },

        textBtnClass(v){
            if(v==0){return 'm_gray'}
            if(v==1){return 'm_green'}
        },

        dialogTitle(v,name){
            if(!!v){
                return '编辑'+name
            }
            return '新增'+name

        },

        

        time(v) {
            let t = v || ''
            return (t + '').slice(0, 10)
        },

        imagesStrArray(v){
            if(v){
             let array = JSON.parse(v)
             return array[0] || ''
            }
            return 'v'

        },

        order_status_class(v){
            if(v=='1'){return 'm_red'}
            if(v=='3'){return 'm_green'}
            if(v=='0'){return 'm_org'}
            if(v=='-1'){return 'm_gray'}
          
        }

        

    }
}



