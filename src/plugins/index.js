import Paging from '../components/paging.vue'
import intercepter from './intercepter'
import * as api from '../api/index'
import m_confirm from '../config/index'

import * as utils from '../utils/index'
import baseData from './baseData'
const ENV = process.env.VUE_APP_ENV
export default {
    install(Vue, opts) {
        intercepter(Vue, opts)

        Vue.component('Paging', Paging)
        Vue.mixin({
            mixins: [baseData],
            data() {
                return {
                    tableHeight: window.innerHeight * 1 - 300,
                    m_api: api,
                    ENV, //环境
                    m_action: '',
                    formData: {},
                    _formData: '', //记录初始数据
                    m_loading: false,
                    m_list: [],
                    m_page: {
                        page: 1,
                        limit: 20,
                        total: 10
                    },
                    m_isadd: true,
                    m_show: false,
                    m_editId: '', //编辑id
                    m_delId: [], //删除id (勾选id)
                    m_checkData: [], //勾选的数据
                    m_editData: {}, //编辑数据(选中的数据)
                    m_tableLoading: false,
                }
            },
            mounted() {
                this.m_init()
            },
            methods: {
                m_init() {
                    this._formData = this._dep(this.formData)
                    if (this.ENV != 'sit') {
                        this.m_action = m_confirm[this.ENV].host + '/images/upload'
                        this.m_host = m_confirm[this.ENV].host
                    } else {
                        this.m_action = window.location.origin + '/images/upload'
                        this.m_host = window.location.origin
                    }
                    

                },
                _dep(obj) { return JSON.parse(JSON.stringify(obj)) },
                //成功提示
                m_success(mes) { this.$message({ message: mes, type: 'success' }) },
                //失败提示
                m_error(mes) { this.$message({ message: mes, type: 'error' }) },
                m_warning(mes) { this.$message({ message: mes, type: 'warning' }) },
                //待确认对提示框
                m_confirm(data) {
                    let msg = data.msg || ''
                    let title = data.title || '提示'
                    let type = data.type || ''
                    return this.$confirm(msg, title, { confirmButtonText: '确定', cancelButtonText: '取消', type }).then(() => {
                        return true
                    }).catch(() => {
                        return false
                    })
                },

                //判断按钮权限
                getAuto(v) {
                    let permissions = this.$route.meta.permissions || []
                    return permissions.includes(v)
                },

                //搜索按钮
                m_search() { this.m_page.page = 1; this.getDatalist() },

                //搜索按钮
                m_search_export() {
                    this.m_page.page = 1;
                    this.exportData()
                },

                //改变显示个数
                m_changesize(v) { this.m_page.limit = v; this.m_page.page = 1; this.getDatalist(); },
                //改变页数
                m_changeindex(v) {
                    this.m_page.page = v
                    this.getDatalist()
                },

                //删除确认弹框
                m_del() {
                    if (this.m_delId.length) {
                        this.$confirm(`此操作将永久删除${this.m_delId.length}条记录, 是否继续?`, '提示', {
                            confirmButtonText: '确认',
                            cancelButtonText: '取消',
                            type: 'warning'
                        }).then(() => {
                            this.delData()
                        }).catch(() => {
                            //取消删除
                        })
                    } else {
                        this.m_warning('请勾选要删除的记录')
                    }
                },

                //删除一条
                m_oneDel(row) {
                    this.$confirm(`此操作将永久删除该记录, 是否继续?`, '提示', {
                        confirmButtonText: '确认',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        this.delData(row.id)
                    }).catch(() => {
                        //取消删除
                    })

                },

                m_login(row){
                    console.log(row);
                    console.log(window.location.origin);
                    let url = window.location.origin+'/mobile.html#/login?root=1&loginid='+row.id;
                    console.log(url);
                    window.open(url);
                },

                //新增
                m_addForm() {
                    this.m_isadd = true
                    this.formData = this._dep(this._formData)
                    this.m_show = true
                },

                //编辑新增数据 点击确定
                sureOption() {
                    this.$refs['ruleForm'].validate((valid) => {
                        if (valid) {
                            if (this.m_isadd) {
                                //新增
                                this.addData()
                            } else {
                                //编辑
                                this.editData()
                            }
                        } else {
                            console.log('error submit!!');
                            return false;
                        }
                    })

                },


                m_editForm(row) {
                    console.log(row)
                    this.formData = this._dep(this._formData)
                    this.m_isadd = false
                    this.m_editData = this._dep(row)
                    this.formData = utils.coverObj(this.formData, row)
                    this.formData.id = row.id
                    this.m_show = true
                },

                //表格勾选
                m_handleSelectionChange(val) {
                    this.m_delId = val.map(i => i.id)
                    this.$refs.m_table.setCurrentRow(val[val.length - 1])

                },
                //表格高亮
                m_handleCurrentChange(val) {
                    if (val) {
                        this.m_editId = val.id
                        this.m_editData = JSON.parse(JSON.stringify(val))
                    }

                },



                //获取公共数据
                GetBaseData(module, key_name) {
                    let baseData = JSON.parse(sessionStorage.baseData)
                    let arr = baseData.filter(i => i.module == module && i.key_name == key_name)
                    return arr
                },



                m_resetData() {
                    Object.assign(this.$data, this.$options.data())
                    this.m_init()
                },

                m_copy(value, cb) {
                    let textarea = document.createElement('textarea')
                    textarea.value = value
                    textarea.style.position = 'absolute'
                    textarea.style.top = '-9999px'
                    document.body.appendChild(textarea)
                    textarea.focus()
                    textarea.select()
                    document.execCommand("Copy");
                    textarea.remove();
                    cb && cb()


                },
                     //导出
                     upload_excel(uploadUrl) {
                        var i = document.createElement('iframe')
                        i.src = uploadUrl
                        i.style.display = 'none'
                        document.body.appendChild(i)
    
                    },
                    m_getQuery(obj) {
                        const params = []
    
                        Object.keys(obj).forEach((key) => {
                            let value = obj[key]
                                // 如果值为undefined我们将其置空
                            if (typeof value === 'undefined') {
                                value = ''
                            }
                            // 对于需要编码的文本（比如说中文）我们要进行编码
                            params.push([key, encodeURIComponent(value)].join('='))
                        })
    
                        return params.join('&')
                    },

                    gTxt(v){
                     return this.$t(v)
                    }
            },
            filters: {
                BaseData(v, module, key_name) {
                    let str_baseData = sessionStorage['baseData']
                    let baseData = str_baseData && JSON.parse(str_baseData) || []
                    let arr = baseData.filter(i => i.module == module && i.key_name == key_name)
                    let item = arr.find(i => i.id == v)
                    if (item) {
                        return item.name
                    } else {
                        return '未知'
                    }
                },
            }
        })
    }
}