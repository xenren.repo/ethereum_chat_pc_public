import { constantRouterMap } from '@/router'
import _import from '../../router/_import'



function addRouter(menulist) {
    let menu = JSON.parse(menulist)
    console.log('menu');
    console.log(menu);
    let navlist = []
    for (let i in menu) {
        if (menu[i].children && menu[i].children.length !== 0) {
            navlist.push({
                name: menu[i].name,
                path: menu[i].path,
                component: _import('index'),
                icon: menu[i].icon,
                radius: menu[i].is_hidden==0,
                // icon: 'el-icon-location',
                children: []
            })
            for (let j in menu[i].children) {
                if (menu[i].children[j].children && menu[i].children[j].children.length !== 0) {
                    navlist[navlist.length - 1].children.push({
                        path: menu[i].children[j].path,
                        name: menu[i].children[j].name,
                        radius:menu[i].children[j].is_hidden==0,
                        component: _import('layer'),
                        children: []
                    })
                    for (let k in menu[i].children[j].children) {
                        navlist[navlist.length - 1].children[navlist[navlist.length - 1].children.length - 1].children.push({
                            path: menu[i].children[j].children[k].path,
                            name: menu[i].children[j].children[k].name,
                            radius:menu[i].children[j].children[k].is_hidden==0,
                            component: _import(menu[i].children[j].children[k].rel),
                            meta: { permissions: menu[i].children[j].children[k].permissions ? menu[i].children[j].children[k].permissions : [] }
                        })
                    }
                } else {
                    navlist[navlist.length - 1].children.push({
                        path: menu[i].children[j].path,
                        name: menu[i].children[j].name,
                        radius:menu[i].children[j].is_hidden==0,
                        component: _import(menu[i].children[j].rel),
                        children: [],
                        meta: { permissions: menu[i].children[j].permissions ? menu[i].children[j].permissions : [] },
                    })
                }
            }
        } else {
            navlist.push({
                path: menu[i].path,
                component: _import('index'),
                radius: menu[i].is_hidden == 0,
                redirect: menu[i].path + '/index',
                icon: menu[i].icon,
                children: [{ path: 'index', name: menu[i].name,radius: menu[i].is_hidden == 0, component: _import(menu[i].rel), meta: { permissions: menu[i].permissions ? menu[i].permissions : [] } }]
            })
        }
    }
    navlist.unshift({
        path: '/index',
        component: _import('index'),
        radius: true,
        redirect: '/index/index',
        icon: 'shouye',
        children: [{ path: 'index', radius:true, name: '首页', component: _import('home/homepage') }]
    })
    navlist.push({
        path: '/404',
        name:'404',
        component: _import('errorPage/404')
    })

    navlist.push({
        path: '*',

        redirect: '/404'
    })
    return navlist
}

const permission = {
    state: {
        addRouters: sessionStorage.getItem('addRouters') ? JSON.parse(sessionStorage.getItem('addRouters')) : []
    },
    mutations: {
        SET_ROUTERS: (state, routers) => {
            sessionStorage.setItem('addRouters', JSON.stringify(routers))
            state.addRouters = routers
        }
    },
    actions: {
        GenerateRoutes({ commit }, data) {
            return new Promise(resolve => {
                var accessedRouters = addRouter(JSON.stringify(data.data))
                commit('SET_ROUTERS', data)
                resolve(accessedRouters)
            })
        }
    }
}

export default permission