import Vuex from 'vuex'
import Vue from 'vue'
import axios from 'axios'
import permission from './modules/permission'
import * as api from '@/api/public'

Vue.use(Vuex)

let state = {
    loading: false,
    routerList: sessionStorage['routerList'] ? JSON.parse(sessionStorage['routerList']) : [],
    viewTagList: sessionStorage['viewTagList'] ? JSON.parse(sessionStorage['viewTagList']) : [], //tab标签

    rolemenupc_list: [], //pc权限菜单列表
    rolemenuapp_list: [], //app权限菜单列表
    isLogin: sessionStorage['isLogin'] ? sessionStorage['isLogin'] : 0,
    levelList:sessionStorage['levelList'] ? JSON.parse(sessionStorage['levelList']) : [],
    payTypeList:sessionStorage['payTypeList'] ? JSON.parse(sessionStorage['payTypeList']) : [],
    orderTypeList:sessionStorage['orderTypeList'] ? JSON.parse(sessionStorage['orderTypeList']) : [],
    

}

let actions = {
    

    setLoading({ commit }, info) {
        commit('SET_LOADING', info)
    },
    setRouterList({ commit }, info) {
        commit('SET_ROUTER_LIST', info)
    },
    setViewTagList({ commit }, info) {
        commit('SET_VIEW_TAG_LIST', info)
    },
    setrolemenupc({ commit }, info) {
        return new Promise((resolve, reject) => {
            api.getAllMenuList(0).then(_ => {
                commit('SET_ROLEMENUPC_LIST', _.data)
                resolve()
            }).catch(_ => {
                reject()
            })
        })
    },
    setrolemenuapp({ commit }, info) {
        return new Promise((resolve, reject) => {
            api.getAllMenuList(1).then(_ => {
                commit('SET_ROLEMENUAPP_LIST', _.data)
                resolve()
            }).catch(_ => {
                reject()
            })
        })
    },
    setIsLogin({ commit }, info) {
        return new Promise(resolve => {
            commit('SET_IS_LOGIN', info)
            resolve()
        })
    },

    setLevelList({ commit }, info) {
        commit('SET_LEVEL_LIST', info)
    },
    setPayTypelList({ commit }, info) {
        commit('SET_PAY_TYPE_LIST', info)
    },
    setOrderTypeList({ commit }, info) {
        commit('SET_ORDER_TYPE_LIST', info)
    },



}

let mutations = {
    SET_LOADING(state, info) {
        state.loading = info
    },
    SET_ORG_LIST(state, info) {
        state.org_list = info
    },

    SET_ROLEMENUPC_LIST(state, info) {
        state.rolemenupc_list = info
    },
    SET_ROLEMENUAPP_LIST(state, info) {
        state.rolemenuapp_list = info
    },
    SET_ROUTER_LIST(state, info) {
        sessionStorage['routerList'] = JSON.stringify(info)
        state.routerList = info
    },
    SET_VIEW_TAG_LIST(state, info) {
        sessionStorage['viewTagList'] = JSON.stringify(info)
        state.viewTagList = info
    },
    SET_IS_LOGIN(state, info) {
        sessionStorage['isLogin'] = info
        state.isLogin = info
    },

    SET_LEVEL_LIST(state, info) {
        sessionStorage['levelList'] = JSON.stringify(info)
        state.levelList = info
    },
    SET_PAY_TYPE_LIST(state, info) {
        sessionStorage['payTypeList'] = JSON.stringify(info)
        state.payTypeList = info
    },
    SET_ORDER_TYPE_LIST(state, info) {
        sessionStorage['orderTypeList'] = JSON.stringify(info)
        state.orderTypeList = info
    },
   
}
let getters = {
    loading: state => state.loading,
    routerList: state => state.routerList,
    viewTagList: state => state.viewTagList,
    isLogin: state => state.isLogin,
    

    group_list: state => state.group_list,
    rolemenupc_list: state => state.rolemenupc_list,
    rolemenuapp_list: state => state.rolemenuapp_list,
    addRouters: state => state.permission.addRouters,
    levelList: state => state.levelList,
    payTypeList:state => state.payTypeList,
    orderTypeList:state => state.orderTypeList,
}

export default new Vuex.Store({
    state,
    actions,
    mutations,
    modules: {
        permission
    },
    getters
})