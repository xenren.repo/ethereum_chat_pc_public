import axios from 'axios'

export const register = (data) => {
    return axios.post("/seller/auth/register", data)
}

export const mobile_verify = (data) => {
    return axios.post("/seller/auth/mobile-verify", data)
}