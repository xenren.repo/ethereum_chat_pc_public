import axios from 'axios'

// 登陆
export const login = (data) => {
    return axios.post("/admin/login", data)
}

export const logout = () => {
    return axios.get('/admin/logout', {})
}

export const editpassword = (data) => {
    return axios.post('/admin/change_password', data)
}


//数据字典
export const data_dictionary = (data) => {
    return axios.post('/data_dictionary', data)
}




//缓存清除
export const cache_clear = () => {
    return axios.get('/cache_clear', {})
}