import axios from 'axios'
import { parse } from 'error-stack-parser';
//获取所有菜单
export const getAllMenuList = (data) => { 
    return axios.get('/modules', {params:{ 'client_type': data }})
}
//获取有权限菜单
export const getMenuList = () => { 
    return axios.get('/module/auth',{ params:{ 'client_type': 0 }})
}
//新增菜单模块
export const addMenuModule = (data) => { 
    return axios.post('/module/add', data)
}
//修改菜单模块
export const updateMenuModule = (data) => { 
    return axios.post('/module/update', data)
}
//删除菜单模块
export const deleteMenuModule = (data) => { 
    return axios.post('/module/delete', { 'id': data })
}

//操作员列表
export const operators = (data) => { 
    return axios.get('/admins', {params:data})
}

//操作员添加
export const add_operators = (data) => { 
    return axios.post('/admin/add', data)
}

//操作员编辑
export const edit_operators = (data) => { 
    return axios.post('/admin/update', data)
}

//操作员删除
export const del_operators = (data) => { 
    return axios.post('/admin/delete', data)
}

//获取角色分类
export const getRoleCategory = (data) => { 
    return axios.get('/role_categories', data)
}



//获取某用户当前权限
export const get_user_permission = (data) => { 
    return axios.get('/user_permissions', {params:data})
}


//设置用户权限
export const set_user_permission = (data) => { 
    return axios.post('/user_permission/set', data)
}


//系统设置列表
export const system_set = (data) => { 
    return axios.get('/settings')
}

//系统设置更新
export const system_set_edit = (data) => { 
    return axios.post('/setting/update', data)
}




































