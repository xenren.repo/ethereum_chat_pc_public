import axios from 'axios'



//新消息提醒
export const new_info = (data) => {
    return axios.get('/notice/new_info', { params: data })
}

//公告列表
export const sys_info = (data) => {
    return axios.get('/sys_info', { params: data })
}

//公告详情
export const sys_info_show = (data) => {
    return axios.get('/sys_info/show', { params: data })
}


//公告添加
export const sys_info_add = (data) => {
    return axios.post('/sys_info/add', data)
}

//公告删除
export const sys_info_delete = (id) => {
    return axios.post('/sys_info/delete', { id })
}

//公告编辑
export const sys_info_update = (data) => {
    return axios.post('/sys_info/update', data)
}

//会员列表
export const members = (data) => {
    return axios.get('/users', { params: data })
}

//会员添加
export const members_add = (data) => {
    return axios.post('/user/add', data)
}

//会员删除
export const members_del = (id) => {
    return axios.post('/user/delete', { id })
}

export const update_reward_mode = (data) => { 
    return axios.post('/goods/reward_mode', data)
}
//会员编辑
export const members_edit = (data) => {
    let d = JSON.parse(JSON.stringify(data))
    return axios.post('/user/update', d)
}

//会员解冻
export const unfreeze = (data) => {
    return axios.post('/user/set_status', data)
}

//金额设置
export const set_credit = (data) => {
    return axios.post('/account_record/set_credit', data)
}

//订单管理
export const orders = (data) => {
    return axios.get('/orders', { params: data })
}







////////////////
//导航图标新增
export const navigation_add = (data) => {
    return axios.get('/navigation/add', { params: data })
}

//导航图标删除
export const navigation_delete = (id) => {
    return axios.post('/navigation/delete',  { id })
}

//导航图标编辑
export const navigation_update = (data) => {
    return axios.post('/navigation/update', data)
}

//导航图标管理列表
export const navigation = (data) => {
    return axios.post('/navigation', data)
}


//首页轮播图列表
export const ad = (data) => {
    return axios.get('/ad', { params: data })
}

//首页轮播图修改
export const ad_update = (data) => {
    return axios.post('/ad/update', data)
}

//首页轮播图添加
export const ad_add = (data) => {
    return axios.post('/ad/add', data)
}

//轮播图删除
export const ad_del = (id) => {
    return axios.post('/ad/delete', { id })
}

//广告列表
export const third_ad = (data) => {
    return axios.get('/third_ad', { params: data })
}

//广告添加
export const third_ad_add = (data) => {
    return axios.post('/third_ad/add', data)
}


//广告编辑
export const third_ad_update = (data) => {
    return axios.post('/third_ad/update', data)
}

//广告删除
export const third_ad_delete = (id) => {
    return axios.post('/third_ad/delete',  { id })
}

//广告位(所有数据 json格式)
export const ad_position = (data) => {
    return axios.get('/ad_position', { params: data })
}

//产品分类列表
export const goods_categories = (data) => {
    return axios.get('/goods_categories', { params: data })
}

//产品分类编辑
export const goods_category_update = (data) => {
    return axios.post('/goods_category/update', data)
}

//产品分类添加
export const goods_category_add = (data) => {
    return axios.post('/goods_category/add', data)
}

//产品分类删除
export const goods_category_delete = (id) => {
    return axios.post('/goods_category/delete', { id })
}

//产品列表
export const goods = (data) => {
    return axios.get('/goods', { params: data })
}

//产品编辑
export const goods_update = (data) => {
    return axios.post('/goods/update', data)
}

//产品添加
export const goods_add = (data) => {
    return axios.post('/goods/add', data)
}

//产品删除
export const goods_delete = (id) => {
    return axios.post('/goods/delete',  { id })
}

//产品单项设置
export const goods_set_status = (data) => {
    return axios.post('/goods/set_status', data)
}

//产品详情
export const goods_show = (data) => {
    return axios.post('/goods/show', data)
}

//关闭订单
export const order_close = (data) => {
    return axios.post('/order/close', data)
}

//订单详情
export const order_detail = (data) => {
    return axios.post('/order/detail', data)
}

//修改地址
export const order_set_address = (data) => {
    return axios.post('/order/set_address', data)
}


export const validate_user = (data) => {
    return axios.post('/admin/seller_validate', data)
}

//发货
export const order_deliver = (data) => {
    return axios.post('/order/deliver', data)
}

//快递列表
export const express_list = (data) => {
    return axios.post('/express_list', data)
}

//查看包裹物流
export const order_package = (data) => {
    return axios.post('/order_package/detail', data)
}

//查看包裹物流
export const order_package_trace = (data) => {
    return axios.post('/order_package/dev/trace', data)
}

//修改运费
export const set_express_cost = (data) => {
    return axios.post('/order/set_express_cost', data)
}

//修改订单总价格
export const total_amount_set = (data) => {
    return axios.post('/order/total_amount/set', data)
}

//设置订单支付状态
export const system_pay = (data) => {
    return axios.post('/order/system_pay', data)
}

//评价列表
export const order_comment = (data) => {
    return axios.get('/order_comment',{ params :data })
}

//评价审核
export const comment_set_status = (data) => {
    return axios.post('/order_comment/set_status', data)
}

//余额使用明细
export const finance_credit1 = (data) => {
    return axios.post('/finance/credit1', data)
}


//余额使用明细
export const finance_credit1_export = (data) => {
    return axios.post('/finance/credit1/export', data)
}

//余额使用明细
export const generate_seller_order = (data) => {
    return axios.post('/order/generate_order', data)
}

export const seller_order_paid = (data) => {
    return axios.post('/order/alipay_seller_order_paid', data)
}

export const pay_order_seller = (data, confg) => {
    return axios.post('/order/alipay_seller_order', data, confg)
}

export const finance_withdraw = (data) => {
    return axios.post('/finance/withdraw', data)
}

export const credit1 = (data) => {
    return axios.post('/finance/creditOne', data)
}

export const finance_withdraw_approve = (data) => {
    return axios.post('/finance/withdraw/approve', data)
}

export const finance_withdraw_new = (data) => {
    return axios.post('/finance/withdraw/create', data)
}

//维权处理
export const refund_set_status = (data) => {
    return axios.post('/order/refund/set_status', data)
}

//退款进度详情
export const refund_detail = (data) => {
    return axios.post('/order/refund/detail', data)
}


//专用券明细
export const finance_credit2 = (data) => {
    return axios.get('/finance/credit2',{ params :data })
}

//专用券明细
export const recharge = (data) => {
    return axios.get('/recharge',{ params :data })
}

//充值申请处理
export const recharge_set_status = (data) => {
    return axios.post('/recharge/set_status', data)
}

//产品抓取
export const goods_fetch = (data) => {
    return axios.post('/goods/fetch', data)
}


//品牌券明细
export const downgrade = (data) => {
    return axios.get('/order/downgrade',{ params :data })
}

export const deposit_withdraw_request = (data) => {
    return axios.post('/order/deposit_withdraw_request',data)
}

export const payment_success = (data) => {
    return axios.get('/order/seller_order_paid',{ params :data })
}

export const seller_transactions = (data) => {
    return axios.get('/order/alipay_seller_order_history',{ params :data })
}

export const seller_transactions_refunds = (data) => {
    return axios.get('/order/seller_transactions_refunds',{ params :data })
}

export const seller_transactions_all = (data) => {
    return axios.get('/order/alipay_seller_order_history_all',{ params :data })
}

export const get_all_sellers = (data) => {
    return axios.get('/order/get_all_sellers',{ params :data })
}

export const update_seller = (data) => {
    return axios.post('/order/update_sellers',data)
}

export const refund_seller = (data) => {
    return axios.post('/order/admin_refund_request', data)
}

export const finance_credit3 = (data) => {
    return axios.get('/finance/credit3',{ params :data })
}

export const medium = (data) => {
    return axios.get('medium', {params: data})
}

export const markup = (data) => {
    return axios.get('markup', {params: data})
}

export const receipts = () => {
    return axios.get('receipts', {params: {}})
}

export const markup_update = (data) => {
    return axios.post('markup/update', data)
}

export const markup_update_prices = (data) => {
    return axios.post('markup/updatePrices', data)
}

export const mall_info = (data) => {
    return axios.post('user/mall/info', data)
}

export const medium_show = (data) => {
    return axios.get('medium/show', {params: data})
}

export const medium_delete = (id) => {
    return axios.post('medium/delete', {id})
}

export const medium_add = (data) => {
    return axios.post('medium/add', data)
}

export const medium_update = (data) => {
    return axios.post('medium/update', data)
}

export const seller_approval_list = (data) => {
    return axios.get('/seller/approval/list', { params: data })
}
export const seller_give_action = (data) => {
    return axios.post('/seller/approval/status', data)
}
//评价审核
export const comment_set_reply = (data) => {
    return axios.post('/order_comment/reply', data)
}

export const images_upload = (data) => {
    return axios.post('/images/upload', data)
}
export const report_seller = (data) => {
    return axios.post('/seller/report/list', data)
}
export const reject_seller = (data) => {
    return axios.post('/admin/seller/reject', data)
}

export const reject_seller_update = (data) => { 
    return axios.post('/seller/approval/update', data)
}

export const cheatsheet_get_payment = (data) => {
    return axios.get('cheatsheet/payment')
}

export const cheatsheet_set_payment = (data) => {
    return axios.post('cheatsheet/payment/set', data)
}

export const cheatsheet_get_seller = (data) => {
    return axios.get('cheatsheet/seller')
}

export const cheatsheet_set_seller = (data) => {
    return axios.post('cheatsheet/seller/set', data)
}

export const cheatsheet_get_topup_lists = (data) => {
    return axios.get('cheatsheet/seller_topup')
}

export const cheatsheet_set_topup_lists = (data) => {
    return axios.post('cheatsheet/seller_topup/set', data)
}
export const cheatsheet_delete_topup_lists = (data) => {
    return axios.post('cheatsheet/seller_topup/delete', data)
}


// Seller Top up
export const seller_topup_lists = (data) => {
    return axios.post('seller/topup/list', data)
}

// Seller Top up
export const upload_csv = (data) => {
    return axios.post('order/import-csv', data)
}


