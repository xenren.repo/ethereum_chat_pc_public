import Vue from 'vue'
import Router from 'vue-router'

import _import from './_import'

Vue.use(Router)

export default new Router({
  routes: [
    { path: '/', redirect: '/login'},
    { path: '/404', component: _import('errorPage/404')},
    { path: '/401', component: _import('errorPage/401')},
    { path: '/login',name: 'login', component: _import('login/index')},
    { path: '/register-seller',name: 'sellerRegister', component: _import('login/register')},
    //{ path: '/register-seller/term-conditions',name: 'termCondition', component: _import('')}
  ]
})
