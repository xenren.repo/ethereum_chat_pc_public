import router from './'
import store from '../store'
//路由拦截
let registerRouteFresh = true
router.beforeEach((to, from, next) => {    
        next()
  })

  router.onReady(function() {              // 刷新之后挂载路由会被清除  需要重新挂载
    if (registerRouteFresh ) {
      const roles = store.getters.addRouters
      store.dispatch('GenerateRoutes', roles).then((response) => {
        // console.log(response)
        router.addRoutes(response)
      })
    }
})
