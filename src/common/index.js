import { requireFolderName } from '@/utils' // 带日期选择操作的边框
const components = requireFolderName(require.context('./', true, /^\.\/[\s\S]+\/index.vue$/), ['./index.js'], 'vue')
export default components
