import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)

const OPEN = true

import en_US from './src/en'
import zh_TW from './src/tw'
import zh_CN from './src/zh'

import el_en_US from 'element-ui/lib/locale/lang/en'
import el_zh_CN from 'element-ui/lib/locale/lang/zh-CN'
import el_zh_TW from 'element-ui/lib/locale/lang/zh-TW'

const messages = {
  'cn': {...zh_CN,...el_zh_CN},   // 中文语言包
  'tw': {...zh_TW,...el_zh_TW},   // 繁体语言包
  'en':{...en_US,...el_en_US}   // 英文语言包
}





export  const  i18n =  new VueI18n({
  locale : 'cn', // set locale 默认显示
  messages : messages // set locale messages
})

//改变语言
export const changeLang = (v) =>{
    i18n.locale  =  v
}


export const txt = function (v) {
   return OPEN? this.$t(v) : v
 }



window.changeLang = changeLang