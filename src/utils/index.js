

// obj1 的值覆盖到obj上 , （编辑表单时候用）
export const coverObj = (obj, obj1) => {
    let o = JSON.parse(JSON.stringify(obj))
    let o_key = Object.keys(o)
    o_key.forEach(el=>{
        o[el] = obj1[el] || o[el]
    })
    return o
}



export function param2Obj(url) {
    const search = url.split('?')[1]
    if (!search) {
        return {}
    }
    return JSON.parse(
        '{"' +
        decodeURIComponent(search)
            .replace(/"/g, '\\"')
            .replace(/&/g, '","')
            .replace(/=/g, '":"')
            .replace(/\+/g, ' ') +
        '"}'
    )
}

export const eachOwn = (obj, fn) => {
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            fn(obj[key], key, obj)
        }
    }
}

// 组件
export const requireFolderName = (r, exinclude) => {
    const contents = {}
    const paths = r.keys().filter(p => {
        return exinclude.indexOf(p) === -1
    })
    for (const p of paths) {
        const fn = r(p).default ? r(p).default : r(p)
        const _array = p.split('/')
        const index = _array.findIndex(item => item === 'index.vue')
        contents[_array[index - 1]] = fn
    }
    return contents
}


export const lazyComponent = name => resolve => require([`@/views/${name}`], resolve)
